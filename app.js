//requiring instance of express.gives access to express api
const express = require('express');

//function declaration
const app = express();

app.set('view engine', 'pug');

app.get('/', (request, response) => {
    response.render('index');
});

app.get('/account', (request, response) => {
    response.render('account', { money: '$100', recentTransaction: true });
});

//starts new server on specific port and add callback
app.listen(3000, () => {
    console.log("Listening on port 3000.");
});